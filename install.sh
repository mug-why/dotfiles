#!/usr/bin/env bash
#  Script creates symbolic links for configs from $HOME dir to $HOME/dotfiles
#+ so that updates from git automatically apply. This means that repository
#+ has to be cloned to $HOME/dotfiles dir.

__err() { echo "$1"; exit 1; }
command -v tar &>/dev/null || __err "tar not found"
cd "${HOME}" || __err "Can't cd to \$HOME"

# List of config files
DOTFILES=('.bashrc' '.inputrc' '.mostrc' '.nanorc' '.tmux.conf' '.config/htop/htoprc')
echo " >> List of dotfiles: ${DOTFILES[*]}"

# Create backup archive or directory of existing files and links
AR="dotfiles_$(date +%Y.%m.%d_%H.%M.%S).tar.gz"
echo " >> Creating backup archive ${AR}."
tar --remove-files -czhvf "${AR}" "${DOTFILES[@]}"

#  Create '~/.nano/.backup' directory. It's configured in .nanorc
#+ to use as a folder for backup files
mkdir -pv "${HOME}/.nano/.backup"

# Create symbolic links
echo " >> Creating symbolic links."
for f in "${DOTFILES[@]}"; do
  # Create '../' prefix for files in subfolders
  p=${f//[^\/]/}
  p=${p//\//\.\.\/}
  f_d=$(dirname "${f}")
  [ ! -d "${f_d}" ] && mkdir -pv "${f_d}"
  # Create link
  ln -s "${p}dotfiles/${f}" "${HOME}/${f}"
  stat -c %N "${f}"
done
