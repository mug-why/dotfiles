# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Update paths for compatibility
PATH=$PATH:$HOME/bin:/usr/sbin:/sbin:$HOME/.local/bin
export PATH

# Color cli
export CLICOLOR=1

# Nano as viewer and editor
if command -v nano &>/dev/null; then
  export VISUAL=nano
  export EDITOR=nano
fi

#  Uncomment the following line if you don't like systemctl's
#+ auto-paging feature:
#export SYSTEMD_PAGER=

# History settings
HISTIGNORE='ls:bg:fg:history' # ignore some commands
HISTTIMEFORMAT='%F %T ' # time format
HISTCONTROL=ignoreboth  # ignore duplicates and cmds with leading space
HISTSIZE=10000          # history memory limit
HISTFILESIZE=10000      # history file limit
shopt -s histappend     # append to the history file, don't overwrite it
shopt -s cmdhist        # multiline cmd

# Better history with hh (hstr package)
if command -v hh &>/dev/null; then
  # Get more colors
  export HH_CONFIG=hicolor
  # mem/file sync
  export PROMPT_COMMAND="history -a; history -n; ${PROMPT_COMMAND}"
  #  If this is interactive shell, then bind hh to Ctrl-r
  #+ (for Vi mode check doc)
  [[ $- =~ .*i.* ]] && bind '"\C-r": "\C-a hh \C-j"'
else
  # Save each cmd to history immediately
  PROMPT_COMMAND='history -a'
fi

# Additional shell options
shopt -s checkwinsize # check the window size after each command
shopt -s autocd       # enable auto cd by folder names
shopt -s globstar     # enable ** glob

# tmuxp completion
# eval "$(_TMUXP_COMPLETE=source tmuxp)"

# Make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# Use bash-completion, if available
[[ $PS1 && -f /usr/share/bash-completion/bash_completion ]] && \
    . /usr/share/bash-completion/bash_completion

# zsh-like completion (unnecessary?)
# bind 'TAB:menu-complete'

# Enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
  test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" \
                       || eval "$(dircolors -b)"
  alias ls='ls --color=auto'
  #alias dir='dir --color=auto'
  #alias vdir='vdir --color=auto'

  alias grep='grep --color=auto'
  alias fgrep='fgrep --color=auto'
  alias egrep='egrep --color=auto'
fi

# Some more ls aliases
alias ll='ls -lAh'
alias la='ls -A'
alias l='ls '

# My aliases
alias mkdir='mkdir -p -v'
alias mv='mv -iv'
alias rm='rm -Iv --one-file-system --preserve-root'
alias ipt='sudo iptables --line-numbers -nvL'
alias iptt='sudo iptables --line-numbers -nvL IN_public_allow'
alias ipts='sudo iptables -S IN_public_allow'
alias pg='ps aux | grep '
alias spdtest='curl -s https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py | python -'
alias myip='dig +short myip.opendns.com @resolver1.opendns.com'
alias mmtr='sudo mtr -bwi 0.25 -c 100'
alias ports='sudo ss -tunl'
alias ccat='pygmentize -g -O style=colorful'
alias sudo='sudo '
alias S='sudo '

command -v most &>/dev/null && { alias less='most '; export PAGER=most; }
command -v multitail &>/dev/null && alias mtail='multitail '
command -v pydf &>/dev/null && alias df='pydf -h '

# Change colors for st?
LS_COLORS="rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=01;34:st=30;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.axa=00;36:*.oga=00;36:*.spx=00;36:*.xspf=00;36:"
export LS_COLORS

# Settings for interactive shell
if [[ $- =~ .*i.* ]]; then
  # Prompt
  PS1="\[\033[0;37m\]\342\224\214\342\224\200\$([[ \$? != 0 ]] && echo \"[\[\033[0;31m\]\342\234\227\[\033[0;37m\]]\342\224\200\")[$(if [[ ${EUID} == 0 ]]; then echo '\[\033[0;31m\]\h'; else echo '\[\033[1;33m\]\u\[\033[1;37m\]@\[\033[1;34m\]\h'; fi)\[\033[0;37m\]]\342\224\200[\[\033[1;32m\]\w\[\033[0;37m\]]\n\[\033[0;37m\]\342\224\224\342\224\200\342\224\200\342\225\274 \[\033[0m\]"
  # Disable Ctrl+S
  stty -ixon
fi
